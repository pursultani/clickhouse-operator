# ClickHouse operator

This is a simplified replacement for the previous Altinity operator implementation.

Defines a new `ClickHouse` CRD.

## Quick start

### Build

Update the CRD yaml file and build the operator binary.

```bash
$ make manifests build
...
```

### Install

With a working `kubectl` session pointing to a cluster, add the `ClickHouse` CRD and run the operator.

```bash
$ kubectl apply -f ./config/crd/bases/clickhouse.gitlab.com_clickhouses.yaml
customresourcedefinition.apiextensions.k8s.io/clickhouses.clickhouse.gitlab.com created

$ ./bin/manager
```

In a separate tab, deploy an `example` ClickHouse instance to the `default` namespace.

```bash
$ kubectl apply -f ./config/samples/example.yaml
secret/example-users created
clickhouse.clickhouse.gitlab.com/example created
```

Wait a few seconds for pods to be `1/1 Running`.

```bash
$ kubectl get pods -n default
NAME          READY   STATUS    RESTARTS   AGE
example-0-0   1/1     Running   0          33s
example-1-0   1/1     Running   0          33s
example-2-0   1/1     Running   0          33s
```

### Use

Interact with the `example` instance.

```bash
$ kubectl exec -it -n default statefulset/example-0 -- \
    clickhouse-client -f PrettyCompact -q 'SELECT name,engine,data_path FROM system.databases;'
┌─name───────────────┬─engine───┬─data_path─────────────────────────┐
│ INFORMATION_SCHEMA │ Memory   │ /var/lib/clickhouse/              │
│ default            │ Ordinary │ /var/lib/clickhouse/data/default/ │
│ information_schema │ Memory   │ /var/lib/clickhouse/              │
│ system             │ Atomic   │ /var/lib/clickhouse/store/        │
└────────────────────┴──────────┴───────────────────────────────────┘

$ kubectl exec -it -n default statefulset/example-0 -- \
    clickhouse-client -f PrettyCompact -q "SELECT cluster,shard_num,replica_num,host_name,host_address FROM system.clusters WHERE startsWith(host_name, 'example');"
┌─cluster────┬─shard_num─┬─replica_num─┬─host_name─┬─host_address───┐
│ replicated │         1 │           1 │ example-0 │ 192.168.57.139 │
│ replicated │         1 │           2 │ example-1 │ 192.168.35.108 │
│ replicated │         1 │           3 │ example-2 │ 192.168.60.78  │
│ sharded    │         1 │           1 │ example-0 │ 192.168.57.139 │
│ sharded    │         2 │           1 │ example-1 │ 192.168.35.108 │
│ sharded    │         3 │           1 │ example-2 │ 192.168.60.78  │
└────────────┴───────────┴─────────────┴───────────┴────────────────┘
```

### Cleaning up

Delete the `example` instance and the leftover volumes. The associated pods/etc should be destroyed automatically.

```bash
$ kubectl delete clickhouse -n default example
clickhouse.clickhouse.gitlab.com "example" deleted

$ kubectl delete pvc -n default -l 'app=clickhouse,name=example'
persistentvolumeclaim "data-example-0-0" deleted
persistentvolumeclaim "data-example-1-0" deleted
persistentvolumeclaim "data-example-2-0" deleted
persistentvolumeclaim "logs-example-0-0" deleted
persistentvolumeclaim "logs-example-1-0" deleted
persistentvolumeclaim "logs-example-2-0" deleted
```

Tear down the operator with `Ctrl+C`, then delete the CRD.

```bash
$ kubectl delete crd clickhouses.clickhouse.gitlab.com
clickhouse.clickhouse.gitlab.com "example" deleted
```
