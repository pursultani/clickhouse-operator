/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	// K8s Go types defined here:
	// https://github.com/kubernetes/kubernetes/blob/master/pkg/apis/core/types.go
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// ClickHouseDeploy defines configuration for deploying ClickHouse nodes
type ClickHouseDeploy struct {
	// Image is the ClickHouse docker image to be used for nodes.
	Image string `json:"image,omitempty"`

	// Replicas is the number of desired nodes to run.
	// The instance will always be deployed in distributed mode, even if replicas=1.
	Replicas uint `json:"replicas,omitempty"`

	// StorageSize is the amount of block storage capacity to allocate per replica.
	StorageSize resource.Quantity `json:"storageSize,omitempty"`

	// Affinity has any restrictions on replica placement in the cluster.
	// This is copied through to the nodes.
	Affinity *corev1.Affinity `json:"affinity,omitempty"`

	// StorageClass is the storage class to use for data volumes
	StorageClass *string `json:"storageClass,omitempty"`
}

// ClickHouseAdminSecret is similar to SecretKeySelector except with Optional removed.
type ClickHouseAdminSecret struct {
	// Name is the name of the secret
	Name string `json:"name,omitempty"`

	// Key is the entry from the Secret to use
	Key string `json:"key,omitempty"`
}

// ClickHouseAdmin defines a managed admin user to be configured in the ClickHouse instance.
// This is used to define "bootstrap" admin users that can then create other limited users.
type ClickHouseAdmin struct {
	// Name is the username of the user.
	Name string `json:"name,omitempty"`

	// SecretKeyRef points to the secret value where the admin user's password is held.
	// The secret must be in the same K8s namespace as the ClickHouse instance referencing it.
	// This only applies upon user creation and later changes are not automatically synchronized.
	SecretKeyRef ClickHouseAdminSecret `json:"secretKeyRef,omitempty"`
}

// ClickHouseSpec defines the desired state of ClickHouse
type ClickHouseSpec struct {
	// Deploy contains configuration for deploying ClickHouse nodes
	Deploy ClickHouseDeploy `json:"deploy,omitempty"`

	// AdminUsers contains one or more admin users (access_management=1) to create in the instance.
	// Additional non-admin users may be added via ClickHouse APIs using these credentials.
	AdminUsers []ClickHouseAdmin `json:"adminUsers,omitempty"`
}

// ClickHouseStatus defines the observed state of ClickHouse
type ClickHouseStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// ClickHouse is the Schema for the clickhouses API
type ClickHouse struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ClickHouseSpec   `json:"spec,omitempty"`
	Status ClickHouseStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// ClickHouseList contains a list of ClickHouse
type ClickHouseList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ClickHouse `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ClickHouse{}, &ClickHouseList{})
}
