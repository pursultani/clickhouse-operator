/*
Copyright 2022.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"

	"github.com/go-logr/logr"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
)

// ClickHouseReconciler reconciles a ClickHouse object
type ClickHouseReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

type kubernetesResource struct {
	obj     client.Object
	mutator controllerutil.MutateFn
}

const FinalizerName = "clickhouse.opstrace.io/finalizer"
const ServiceAccountName = "clickhouse"
const ClickHouseConfigShasumAnnotationName = "clickhouse-operator/clickhouse-config-shasum"
const ClickHouseConfigMapNameSuffix = "-config"

// Configure permissions to manage ClickHouse CRDs:
//+kubebuilder:rbac:groups=clickhouse.gitlab.com,resources=clickhouses,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=clickhouse.gitlab.com,resources=clickhouses/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=clickhouse.gitlab.com,resources=clickhouses/finalizers,verbs=update

// Configure permissions to deploy ClickHouse instances based on CRDs:
// TODO(nick) for now we are just mimicing deploying StatefulSets, replace with lower-level Pod management?
//+kubebuilder:rbac:groups=,resources=configmaps,verbs=get;list;watch;create;update;patch;delete;scope=Cluster
//+kubebuilder:rbac:groups=,resources=services,verbs=get;list;watch;create;update;patch;delete;scope=Cluster
//+kubebuilder:rbac:groups=apps,resources=statefulsets,verbs=get;list;watch;create;update;patch;delete;scope=Cluster

// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.11.0/pkg/reconcile
func (r *ClickHouseReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("clickhouse", req.NamespacedName)

	crd := &clickhousev1alpha1.ClickHouse{}
	if err := r.Get(ctx, req.NamespacedName, crd); err != nil {
		if !apierrors.IsNotFound(err) {
			log.Error(err, "unable to get ClickHouse")
		}
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	krr := Reconciler{
		scheme: r.Scheme,
		client: r.Client,
		crd:    crd,
		log:    log,
	}
	log.Info("reconciling ClickHouse")

	kr := newClickHouseConfigMaps(req, crd, RenderConfigs(crd))
	kr = append(kr, newClickHouseServices(req, crd)...)
	kr = append(kr, newClickHouseStatefulSets(req, crd)...)
	for _, entry := range kr {
		err := krr.Reconcile(ctx, entry)
		if err != nil {
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *ClickHouseReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&clickhousev1alpha1.ClickHouse{}).
		Complete(r)
}
