package controllers

import (
	"fmt"

	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/util/intstr"
	"k8s.io/utils/pointer"
	ctrl "sigs.k8s.io/controller-runtime"

	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
)

func newClickHouseConfigMaps(
	req ctrl.Request,
	crd *clickhousev1alpha1.ClickHouse,
	configs Configs,
) []*KubernetesResource {
	kr := make([]*KubernetesResource, 0)

	labels := map[string]string{
		"app":  "clickhouse",
		"name": req.Name,
	}

	kr = append(kr, _newConfigMap(
		req.Namespace,
		req.Name,
		labels,
		map[string]string{
			"config.xml": configs.ConfigXML,
			"users.xml":  configs.UsersXML,
		},
	))

	return kr
}

func _newConfigMap(namespace string, name string, labels map[string]string, data map[string]string) *KubernetesResource {
	cm := &corev1.ConfigMap{ObjectMeta: metav1.ObjectMeta{Name: name, Namespace: namespace, Labels: labels}}
	return &KubernetesResource{
		obj: cm,
		ref: &corev1.LocalObjectReference{Name: name},
		mutator: func() error {
			cm.Labels = labels
			cm.Data = data
			return nil
		},
	}
}

func newClickHouseServices(
	req ctrl.Request,
	crd *clickhousev1alpha1.ClickHouse,
) []*KubernetesResource {
	kr := make([]*KubernetesResource, 0)

	clientPorts := []corev1.ServicePort{
		{
			Name:       "http",
			Port:       8123,
			TargetPort: intstr.FromString("http"),
		},
		{
			Name:       "client",
			Port:       9000,
			TargetPort: intstr.FromString("client"),
		},
	}

	nodePorts := append(clientPorts, []corev1.ServicePort{
		{
			Name:       "interserver",
			Port:       9009,
			TargetPort: intstr.FromString("interserver"),
		},
		{
			Name:       "keeper",
			Port:       2181,
			TargetPort: intstr.FromString("keeper"),
		},
		{
			Name:       "raft",
			Port:       9444,
			TargetPort: intstr.FromString("raft"),
		},
		{
			Name:       "metrics",
			Port:       8001,
			TargetPort: intstr.FromString("metrics"),
		},
	}...)

	// Create shared "<name>" Service for access by clients
	clusterLabels := map[string]string{
		"app":  "clickhouse",
		"name": req.Name,
	}
	kr = append(kr, _newService(
		req.Namespace,
		req.Name,
		clusterLabels,
		clientPorts,
		clusterLabels,
		"",
	))

	// Create per-node "<name>-<#>" Services to allow direct addressing between nodes
	for i := uint(0); i < crd.Spec.Deploy.Replicas; i++ {
		nodeLabels := map[string]string{
			"app":     "clickhouse",
			"name":    req.Name,
			"replica": fmt.Sprint(i),
		}
		kr = append(kr, _newService(
			req.Namespace,
			fmt.Sprintf("%s-%d", req.Name, i),
			nodeLabels,
			nodePorts,
			nodeLabels,
			"None",
		))
	}

	return kr
}

func _newService(namespace string, name string, labels map[string]string, ports []corev1.ServicePort, selector map[string]string, clusterIP string) *KubernetesResource {
	svc := &corev1.Service{ObjectMeta: metav1.ObjectMeta{Name: name, Namespace: namespace, Labels: labels}}
	return &KubernetesResource{
		obj: svc,
		ref: &corev1.LocalObjectReference{Name: name},
		mutator: func() error {
			svc.Labels = labels
			svc.Spec.Ports = ports
			svc.Spec.Selector = selector
			if len(clusterIP) > 0 {
				// Avoid triggering a service change if the spec ClusterIP is already assigned
				svc.Spec.ClusterIP = clusterIP
			}
			return nil
		},
	}
}

func newClickHouseStatefulSets(
	req ctrl.Request,
	crd *clickhousev1alpha1.ClickHouse,
) []*KubernetesResource {
	kr := make([]*KubernetesResource, 0)

	// FIXME: Might be possible to replace this with a single StatefulSet?
	for idx := uint(0); idx < crd.Spec.Deploy.Replicas; idx++ {
		// 'replica=#' used by granular per-pod Services
		labels := map[string]string{
			"app":     "clickhouse",
			"name":    req.Name,
			"replica": fmt.Sprint(idx),
		}
		kr = append(kr, _newStatefulSet(
			req.Namespace,
			req.Name,
			idx,
			crd.Spec,
			labels,
		))
	}

	return kr
}

func _newStatefulSet(
	namespace string,
	clusterName string,
	idx uint,
	spec clickhousev1alpha1.ClickHouseSpec,
	labels map[string]string,
) *KubernetesResource {
	stsName := fmt.Sprintf("%s-%d", clusterName, idx)
	sts := &appsv1.StatefulSet{
		ObjectMeta: metav1.ObjectMeta{Name: stsName, Namespace: namespace},
	}

	return &KubernetesResource{
		obj: sts,
		ref: &corev1.LocalObjectReference{Name: stsName},
		mutator: func() error {
			env := make([]corev1.EnvVar, 0)
			// For each admin user, create a password envvar to be referenced by users.xml
			for _, user := range spec.AdminUsers {
				env = append(env, corev1.EnvVar{
					Name: fmt.Sprintf("ADMIN_PASSWORD_%s", user.Name),
					ValueFrom: &corev1.EnvVarSource{
						SecretKeyRef: &corev1.SecretKeySelector{
							LocalObjectReference: corev1.LocalObjectReference{Name: user.SecretKeyRef.Name},
							Key:                  user.SecretKeyRef.Key,
						},
					},
				})
			}
			// Add the pod's replica index, referenced in config.tmpl.xml
			env = append(env, corev1.EnvVar{
				Name:  "REPLICA",
				Value: fmt.Sprint(idx),
			})

			// Each StatefulSet has a matching per-node Service with the same name
			sts.Spec.ServiceName = stsName
			// Each node is being deployed in its own StatefulSet
			var replicas int32 = 1
			sts.Spec.Replicas = &replicas
			sts.Spec.PodManagementPolicy = appsv1.OrderedReadyPodManagement
			sts.Spec.Selector = &metav1.LabelSelector{
				MatchLabels: labels,
			}
			sts.Spec.Template.Labels = labels
			sts.Spec.Template.Spec.Containers = []corev1.Container{
				{
					Name:            "clickhouse",
					Image:           spec.Deploy.Image,
					ImagePullPolicy: corev1.PullIfNotPresent,
					Env:             env,
					Ports: []corev1.ContainerPort{
						{
							Name:          "http",
							ContainerPort: 8123,
						},
						{
							Name:          "client",
							ContainerPort: 9000,
						},
						{
							Name:          "interserver",
							ContainerPort: 9009,
						},
						{
							Name:          "keeper",
							ContainerPort: 2181,
						},
						{
							Name:          "raft",
							ContainerPort: 9444,
						},
						{
							Name:          "metrics",
							ContainerPort: 8001,
						},
					},
					ReadinessProbe: &corev1.Probe{
						ProbeHandler: corev1.ProbeHandler{
							TCPSocket: &corev1.TCPSocketAction{
								Port: intstr.FromString("raft"),
							},
						},
						InitialDelaySeconds: 10,
						TimeoutSeconds:      1,
						PeriodSeconds:       3,
						SuccessThreshold:    1,
						FailureThreshold:    3,
					},
					LivenessProbe: &corev1.Probe{
						ProbeHandler: corev1.ProbeHandler{
							HTTPGet: &corev1.HTTPGetAction{
								Path:   "/ping",
								Port:   intstr.FromString("http"),
								Scheme: corev1.URISchemeHTTP,
							},
						},
						InitialDelaySeconds: 60,
						TimeoutSeconds:      1,
						PeriodSeconds:       3,
						SuccessThreshold:    1,
						FailureThreshold:    10,
					},
					SecurityContext: &corev1.SecurityContext{
						Capabilities: &corev1.Capabilities{
							Add:  []corev1.Capability{"IPC_LOCK", "SYS_NICE"},
							Drop: []corev1.Capability{"ALL"},
						},
					},
					VolumeMounts: []corev1.VolumeMount{
						{
							Name:      "configd",
							MountPath: "/etc/clickhouse-server/config.d",
						},
						{
							Name:      "usersd",
							MountPath: "/etc/clickhouse-server/users.d",
						},
						{
							Name:      "data",
							MountPath: "/var/lib/clickhouse",
						},
						{
							Name:      "logs",
							MountPath: "/var/log/clickhouse-server",
						},
					},
				},
			}
			var uidGid int64 = 101
			sts.Spec.Template.Spec.SecurityContext = &corev1.PodSecurityContext{
				RunAsUser:  &uidGid,
				RunAsGroup: &uidGid,
				FSGroup:    &uidGid,
			}
			sts.Spec.Template.Spec.TerminationGracePeriodSeconds = pointer.Int64Ptr(30)
			sts.Spec.Template.Spec.Volumes = []corev1.Volume{
				{
					Name: "configd",
					VolumeSource: corev1.VolumeSource{
						ConfigMap: &corev1.ConfigMapVolumeSource{
							LocalObjectReference: corev1.LocalObjectReference{Name: clusterName},
							Items: []corev1.KeyToPath{
								{
									Key:  "config.xml",
									Path: "config.xml",
								},
							},
						},
					},
				},
				{
					Name: "usersd",
					VolumeSource: corev1.VolumeSource{
						ConfigMap: &corev1.ConfigMapVolumeSource{
							LocalObjectReference: corev1.LocalObjectReference{Name: clusterName},
							Items: []corev1.KeyToPath{
								{
									Key:  "users.xml",
									Path: "users.xml",
								},
							},
						},
					},
				},
				{
					Name: "data",
					VolumeSource: corev1.VolumeSource{
						PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
							ClaimName: "data",
						},
					},
				},
				{
					Name: "logs",
					VolumeSource: corev1.VolumeSource{
						PersistentVolumeClaim: &corev1.PersistentVolumeClaimVolumeSource{
							ClaimName: "logs",
						},
					},
				},
			}
			sts.Spec.VolumeClaimTemplates = []corev1.PersistentVolumeClaim{
				{
					ObjectMeta: metav1.ObjectMeta{Name: "data"},
					Spec: corev1.PersistentVolumeClaimSpec{
						StorageClassName: spec.Deploy.StorageClass,
						AccessModes:      []corev1.PersistentVolumeAccessMode{corev1.ReadWriteOnce},
						Resources: corev1.ResourceRequirements{
							Requests: corev1.ResourceList{
								corev1.ResourceStorage: spec.Deploy.StorageSize,
							},
						},
					},
				},
				// Separate volume to avoid interruption to logs if data volume is full
				{
					ObjectMeta: metav1.ObjectMeta{Name: "logs"},
					Spec: corev1.PersistentVolumeClaimSpec{
						StorageClassName: spec.Deploy.StorageClass,
						AccessModes:      []corev1.PersistentVolumeAccessMode{corev1.ReadWriteOnce},
						Resources: corev1.ResourceRequirements{
							Requests: corev1.ResourceList{
								corev1.ResourceStorage: resource.MustParse("1Gi"),
							},
						},
					},
				},
			}

			return nil
		},
	}
}
