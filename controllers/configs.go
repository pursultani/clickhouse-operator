package controllers

import (
	"embed"
	"strings"
	"text/template"

	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
)

//go:embed templates/*
var templatesFS embed.FS

var customFuncs template.FuncMap = template.FuncMap{
	// Utility function to use in for loops like this:
	// TODO: this should probably be swapped with using the Helm templating engine as a library
	// "{{ range $i := repeat 3 }}... content ...{{ end }}"
	"repeat": func(count uint) []uint {
		var out []uint
		for i := uint(0); i < count; i++ {
			out = append(out, i)
		}
		return out
	},
}

func render(templates *template.Template, filename string, args *clickhousev1alpha1.ClickHouse) string {
	var statement strings.Builder
	err := templates.ExecuteTemplate(&statement, filename, args)
	if err != nil {
		panic(err)
	}
	return statement.String()
}

type Configs struct {
	ConfigXML string
	UsersXML  string
}

func RenderConfigs(args *clickhousev1alpha1.ClickHouse) Configs {
	templates := template.Must(template.New("configs").Funcs(customFuncs).ParseFS(templatesFS, "templates/*.tmpl.xml")).Funcs(customFuncs)
	return Configs{
		ConfigXML: render(templates, "config.tmpl.xml", args),
		UsersXML:  render(templates, "users.tmpl.xml", args),
	}
}
