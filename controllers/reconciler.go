package controllers

import (
	"context"

	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	clickhousev1alpha1 "gitlab.com/gitlab-org/opstrace/opstrace/clickhouse-operator/api/v1alpha1"
)

type KubernetesResource struct {
	obj     client.Object
	ref     *corev1.LocalObjectReference
	mutator controllerutil.MutateFn
}

type Reconciler struct {
	log    logr.Logger
	client client.Client
	crd    *clickhousev1alpha1.ClickHouse
	scheme *runtime.Scheme
}

func (r *Reconciler) Reconcile(ctx context.Context, kr *KubernetesResource) error {
	obj := kr.obj

	// Set up garbage collection. The object (resource.obj) will be
	// automatically deleted when the owner (clickhouse) is deleted.
	err := controllerutil.SetOwnerReference(r.crd, obj, r.scheme)
	if err != nil {
		r.log.Error(
			err,
			"failed to set owner reference on resource",
			"kind", obj.GetObjectKind().GroupVersionKind().Kind,
			"name", obj.GetName(),
		)
		return err
	}

	op, err := controllerutil.CreateOrUpdate(ctx, r.client, obj, kr.mutator)
	if err != nil {
		r.log.Error(
			err,
			"failed to reconcile resource",
			"kind", obj.GetObjectKind().GroupVersionKind().Kind,
			"name", obj.GetName(),
		)
		return err
	}

	err = r.client.Status().Update(ctx, r.crd)
	if err != nil {
		r.log.Error(
			err,
			"failed to reconcile resource status",
			"kind", obj.GetObjectKind().GroupVersionKind().Kind,
			"name", obj.GetName(),
		)
		return err
	}

	r.log.Info(
		"Reconcile successful",
		"operation", op,
		"kind", obj.GetObjectKind().GroupVersionKind().Kind,
		"name", obj.GetName(),
	)

	return nil
}
